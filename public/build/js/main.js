(self["webpackChunk"] = self["webpackChunk"] || []).push([["js/main"],{

/***/ "./assets/js/main.js":
/*!***************************!*\
  !*** ./assets/js/main.js ***!
  \***************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");

/*==================== SHOW NAVBAR ====================*/
var showMenu = function showMenu(headerToggle, navbarId) {
  var toggleBtn = document.getElementById(headerToggle),
      nav = document.getElementById(navbarId); // Validate that variables exist

  if (headerToggle && navbarId) {
    toggleBtn.addEventListener('click', function () {
      // We add the show-menu class to the div tag with the nav__menu class
      nav.classList.toggle('show-menu'); // change icon

      toggleBtn.classList.toggle('bx-x');
    });
  }
};

showMenu('header-toggle', 'navbar');
/*==================== LINK ACTIVE ====================*/

var linkColor = document.querySelectorAll('.nav__link');

function colorLink() {
  linkColor.forEach(function (l) {
    return l.classList.remove('active');
  });
  this.classList.add('active');
}

linkColor.forEach(function (l) {
  return l.addEventListener('click', colorLink);
});

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ "use strict";
/******/ 
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendors-node_modules_core-js_modules_es_array_for-each_js-node_modules_core-js_modules_web_do-31add8"], () => (__webpack_exec__("./assets/js/main.js")));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvbWFpbi5qcyJdLCJuYW1lcyI6WyJzaG93TWVudSIsImhlYWRlclRvZ2dsZSIsIm5hdmJhcklkIiwidG9nZ2xlQnRuIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsIm5hdiIsImFkZEV2ZW50TGlzdGVuZXIiLCJjbGFzc0xpc3QiLCJ0b2dnbGUiLCJsaW5rQ29sb3IiLCJxdWVyeVNlbGVjdG9yQWxsIiwiY29sb3JMaW5rIiwiZm9yRWFjaCIsImwiLCJyZW1vdmUiLCJhZGQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0EsSUFBTUEsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBQ0MsWUFBRCxFQUFlQyxRQUFmLEVBQTJCO0FBQ3hDLE1BQU1DLFNBQVMsR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCSixZQUF4QixDQUFsQjtBQUFBLE1BQ0FLLEdBQUcsR0FBR0YsUUFBUSxDQUFDQyxjQUFULENBQXdCSCxRQUF4QixDQUROLENBRHdDLENBSXhDOztBQUNBLE1BQUdELFlBQVksSUFBSUMsUUFBbkIsRUFBNEI7QUFDeEJDLGFBQVMsQ0FBQ0ksZ0JBQVYsQ0FBMkIsT0FBM0IsRUFBb0MsWUFBSTtBQUNwQztBQUNBRCxTQUFHLENBQUNFLFNBQUosQ0FBY0MsTUFBZCxDQUFxQixXQUFyQixFQUZvQyxDQUdwQzs7QUFDQU4sZUFBUyxDQUFDSyxTQUFWLENBQW9CQyxNQUFwQixDQUEyQixNQUEzQjtBQUNILEtBTEQ7QUFNSDtBQUNKLENBYkQ7O0FBY0FULFFBQVEsQ0FBQyxlQUFELEVBQWlCLFFBQWpCLENBQVI7QUFFQTs7QUFDQSxJQUFNVSxTQUFTLEdBQUdOLFFBQVEsQ0FBQ08sZ0JBQVQsQ0FBMEIsWUFBMUIsQ0FBbEI7O0FBRUEsU0FBU0MsU0FBVCxHQUFvQjtBQUNoQkYsV0FBUyxDQUFDRyxPQUFWLENBQWtCLFVBQUFDLENBQUM7QUFBQSxXQUFJQSxDQUFDLENBQUNOLFNBQUYsQ0FBWU8sTUFBWixDQUFtQixRQUFuQixDQUFKO0FBQUEsR0FBbkI7QUFDQSxPQUFLUCxTQUFMLENBQWVRLEdBQWYsQ0FBbUIsUUFBbkI7QUFDSDs7QUFFRE4sU0FBUyxDQUFDRyxPQUFWLENBQWtCLFVBQUFDLENBQUM7QUFBQSxTQUFJQSxDQUFDLENBQUNQLGdCQUFGLENBQW1CLE9BQW5CLEVBQTRCSyxTQUE1QixDQUFKO0FBQUEsQ0FBbkIsRSIsImZpbGUiOiJqcy9tYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyo9PT09PT09PT09PT09PT09PT09PSBTSE9XIE5BVkJBUiA9PT09PT09PT09PT09PT09PT09PSovXHJcbmNvbnN0IHNob3dNZW51ID0gKGhlYWRlclRvZ2dsZSwgbmF2YmFySWQpID0+e1xyXG4gICAgY29uc3QgdG9nZ2xlQnRuID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaGVhZGVyVG9nZ2xlKSxcclxuICAgIG5hdiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKG5hdmJhcklkKVxyXG4gICAgXHJcbiAgICAvLyBWYWxpZGF0ZSB0aGF0IHZhcmlhYmxlcyBleGlzdFxyXG4gICAgaWYoaGVhZGVyVG9nZ2xlICYmIG5hdmJhcklkKXtcclxuICAgICAgICB0b2dnbGVCdG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKT0+e1xyXG4gICAgICAgICAgICAvLyBXZSBhZGQgdGhlIHNob3ctbWVudSBjbGFzcyB0byB0aGUgZGl2IHRhZyB3aXRoIHRoZSBuYXZfX21lbnUgY2xhc3NcclxuICAgICAgICAgICAgbmF2LmNsYXNzTGlzdC50b2dnbGUoJ3Nob3ctbWVudScpXHJcbiAgICAgICAgICAgIC8vIGNoYW5nZSBpY29uXHJcbiAgICAgICAgICAgIHRvZ2dsZUJ0bi5jbGFzc0xpc3QudG9nZ2xlKCdieC14JylcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG59XHJcbnNob3dNZW51KCdoZWFkZXItdG9nZ2xlJywnbmF2YmFyJylcclxuXHJcbi8qPT09PT09PT09PT09PT09PT09PT0gTElOSyBBQ1RJVkUgPT09PT09PT09PT09PT09PT09PT0qL1xyXG5jb25zdCBsaW5rQ29sb3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcubmF2X19saW5rJylcclxuXHJcbmZ1bmN0aW9uIGNvbG9yTGluaygpe1xyXG4gICAgbGlua0NvbG9yLmZvckVhY2gobCA9PiBsLmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpKVxyXG4gICAgdGhpcy5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKVxyXG59XHJcblxyXG5saW5rQ29sb3IuZm9yRWFjaChsID0+IGwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBjb2xvckxpbmspKVxyXG4iXSwic291cmNlUm9vdCI6IiJ9