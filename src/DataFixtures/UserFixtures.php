<?php


namespace App\DataFixtures;

use App\Entity\Administrator;
use App\Entity\Professor;
use App\Entity\Tutor;
use App\Entity\User;
use App\Entity\Student;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserFixtures extends Fixture
{

    public const ADMIN_USER_REFERENCE = 'Admin-user';

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new Administrator();
        $admin->setEmail('chadha.glaid@gmail.com');
        $admin->setUserName('admin');
        $admin->setRoles(["ROLE_ADMIN"]);
        $admin->setCin('12845633');
        $admin->setFirstName('Chadha');
        $admin->setLastName('Glaied');
        $admin->setBirthday(DateTime::createFromFormat('Y-m-d', "1988-08-18"));
        $admin->setGender('Femme');
        $admin->setAddress('Sousse,Rue de lenvironnement');
        $admin->setTel('21328284');
        $admin->setPhoto('');
        $admin->setUpdatedAt(new \DateTime('now'));
        //encodePassword
        $password = $this->encoder->encodePassword($admin, 'admin123');
        $admin->setPassword($password);
        //save the Administrator!
        $manager->persist($admin);
        $manager->flush();


        // other fixtures can get this object using the UserFixtures::ADMIN_USER_REFERENCE constant
        $this->addReference(self::ADMIN_USER_REFERENCE, $admin);


        //************************** an other user with "ROLE_USER"  Professor*****************************************

        $user_Professor = new Professor();
        $user_Professor->setEmail('rouaslama8@gmail.com');
        $user_Professor->setUserName('roua');
        $user_Professor->setRoles(["ROLE_PROFESSOR"]);
        $user_Professor->setCin('12845633');
        $user_Professor->setFirstName('Roua');
        $user_Professor->setLastName('Slama');
        $user_Professor->setBirthday(DateTime::createFromFormat('Y-m-d', "1988-08-08"));
        $user_Professor->setGender('Femme');
        $user_Professor->setAddress('Sousse,Rue de bassatin');
        $user_Professor->setTel('21328285');
        $user_Professor->setPhoto('');
        $user_Professor->setUpdatedAt(new \DateTime('now'));
        //encodePassword
        $password = $this->encoder->encodePassword($user_Professor, '12345678');
        $user_Professor->setPassword($password);
        //save the User!
        $manager->persist($user_Professor);
        $manager->flush();
        //************************** an other user with "ROLE_USER" (Student) ***************************************

        // tutor id can't be null , we can't set it manully cause it's auto generated

        //**************************************    Tutor ***************************************

        $user = new Tutor();

        $user->setEmail('tutor@gmail.com');
        $user->setUserName('tutor');
        $user->setRoles(["ROLE_TUTOR"]);
        $user->setCin('03251487');
        $user->setFirstName('tut');
        $user->setLastName('tutoo');
        $user->setBirthday(DateTime::createFromFormat('Y-m-d', "1987-12-07"));
        $user->setGender('homme');
        $user->setAddress('Sousse,Rue de bassatin');
        $user->setTel('20365987');
        $user->setPhoto('');
        $user->setUpdatedAt(new \DateTime('now'));
        //encodePassword
        $password = $this->encoder->encodePassword($user, '12345678');
        $user->setPassword($password);
        //save the User!
        $manager->persist($user);
        $manager->flush();

    }
}