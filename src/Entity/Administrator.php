<?php

namespace App\Entity;

use App\Repository\AdministratorRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AdministratorRepository::class)
 */
class Administrator extends User implements UserInterface
{
    /**
     * @ORM\Column(type="integer",length=8,unique=true)
     * @Assert\Regex(
     *     pattern="/[0-9]{8}/")
     */
    private $cin;

    public function getCin(): ?int
    {
        return $this->cin;
    }

    public function setCin(int $cin): self
    {
        $this->cin = $cin;

        return $this;
    }
}