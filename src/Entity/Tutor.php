<?php

namespace App\Entity;

use App\Repository\TutorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TutorRepository::class)
 */
class Tutor extends User implements UserInterface
{


    /**
     * @ORM\Column(type="integer",length=8,unique=true)
     * @Assert\Regex(
     *     pattern="/[0-9]{8}/")
     */
    private $cin;

    /**
     * @ORM\OneToMany(targetEntity=Student::class, mappedBy="tutor")
     */
    private $students;

    public function __construct()
    {
        parent::__construct();
        $this->students = new ArrayCollection();
    }


    public function getCin(): ?int
    {
        return $this->cin;
    }

    public function setCin(int $cin): self
    {
        $this->cin = $cin;

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->setTutor($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getTutor() === $this) {
                $student->setTutor(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return '#'.$this->getId().' '.$this->getFirstName().' '.$this->getLastName();
    }


}
