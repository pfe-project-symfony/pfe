<?php

namespace App\Entity;

use App\Repository\ProfessorRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProfessorRepository::class)
 */
class Professor extends User implements UserInterface
{
    /**
     * @ORM\Column(type="integer",length=11, unique=true)
     * @Assert\Regex(
     *     pattern="/[0-9]{8}/",
     *     message="num de téléphone doit se composer de 8 chiffres"
     * )
     */
    private $cin;



    public function getCin(): ?int
    {
        return $this->cin;
    }

    public function setCin(int $cin): self
    {
        $this->cin = $cin;

        return $this;
    }
}
