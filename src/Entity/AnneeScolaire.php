<?php

namespace App\Entity;

use App\Repository\AnneeScolaireRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AnneeScolaireRepository::class)
 */
class AnneeScolaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\Regex(
     *     pattern="/[0-9]{4}/")
     */
    private $debutAnnee;

    /**
     * @ORM\Column(type="smallint")
     *  @Assert\Regex(
     *     pattern="/[0-9]{4}/")
     */
    private $finAnnee;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $libelle;
    //***********************************************************************************************************


    /**
     * @param mixed $id
     */

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDebutAnnee()
    {
        return $this->debutAnnee;
    }

    /**
     * @param mixed $debutAnnee
     */
    public function setDebutAnnee($debutAnnee): void
    {
        $this->debutAnnee = $debutAnnee;
    }

    /**
     * @return mixed
     */
    public function getFinAnnee()
    {
        return $this->finAnnee;
    }

    /**
     * @param mixed $finAnnee
     */
    public function setFinAnnee($finAnnee): void
    {
        $this->finAnnee = $finAnnee;
    }


    public function getLibelle(): ?string
    {
            return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle): void
    {
        $this->libelle = $libelle;
    }

}
