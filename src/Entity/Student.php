<?php

namespace App\Entity;

use App\Repository\StudentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=StudentRepository::class)
 */
class Student extends User implements UserInterface
{

    /**
     * @ORM\ManyToOne(targetEntity=Tutor::class, inversedBy="students")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tutor;

    public function __toString()
    {
        return '#' . $this->getId() . ' ' . $this->getFirstName() . ' ' . $this->getLastName();
    }

    public function getTutor(): ?Tutor
    {
        return $this->tutor;
    }

    public function setTutor(?Tutor $tutor): self
    {
        $this->tutor = $tutor;

        return $this;
    }
    /**
     * @return Tutor|null
     */
    public function getTutorEmail(): ?string
    {
        if (isset($this)) {
            return $this->tutor->getEmail();
        }
    }
}