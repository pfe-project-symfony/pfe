<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/",name="security_login")
     */
    public function login(Request  $request, AuthenticationUtils $authUtils)
    {   // get the login error if there is one

        $error = $authUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();
        //send back the user to the login page(login.html.twig) if connection failed
        return $this->render('security/login.html.twig',[
            'error'         =>$error,
            'lastUsername'  =>$lastUsername
        ]);
    }
    /**
     * @Route("/logout",name="security_logout")
     */
    public function logout(){

    }

    /**
     * @Route("/checkRoles",name="check_roles")
     */

    public function checkRoles() :Response
    {

        if ( $this->getUser()->getRoles()[0] == "ROLE_ADMIN") {
            return $this->redirectToRoute("adminDashboard");
        }
        if ($this->getUser()->getRoles()[0] == "ROLE_STUDENT"){
            return $this->redirectToRoute("studentDashboard");
        }
        if ($this->getUser()->getRoles()[0] == "ROLE_PROFESSOR"){
            return $this->redirectToRoute("professorDashboard");
        }
        if ($this->getUser()->getRoles()[0] == "ROLE_TUTOR"){
            return $this->redirectToRoute("tutorDashboard");
        }
        if ($this->getUser()->getRoles()[0] == "ROLE_HYGIENE_OFFICER"){
            return $this->redirectToRoute("hygOfficerDashboard");
        }




    }







}