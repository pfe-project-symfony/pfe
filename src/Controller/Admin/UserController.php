<?php


namespace App\Controller\Admin;


use App\Services\PasswordGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


///**
// * @Route("/user")
// */
class UserController extends AbstractController
{
    /**
     * @Route("/student",name="studentDashboard")
     */
    public function studentIndex(): Response
    {
        return $this->render('student.html.twig');

    }

    /**
     * @Route("/professor",name="professorDashboard")
     */
    public function professorIndex(): Response
    {
        return $this->render('professor.html.twig');

    }

    /**
     * @Route("/tutor",name="tutorDashboard")
     */
    public function tutorIndex(): Response
<<<<<<< HEAD:src/Controller/Admin/UserController.php
    {
        return $this->render('tutor.html.twig');
=======
    {   return $this->render('base.html.twig');
>>>>>>> f2569ca48c1259e31f9933ed18ec0eebb68601ba:src/Controller/UserController.php

    }

    /**
     * @Route("/hygieneOfficer",name="hygOfficerDashboard")
     */
    public function hygOfficerIndex(): Response
    {
        return $this->render('index.html.twig');

    }

    /**
     * @Route("/test",name="test")
     */

    public function mdp(PasswordGenerator $passwordGenerator): Response
    {
        dd($passwordGenerator->generateRandomStrongPassword(8));
    }


}