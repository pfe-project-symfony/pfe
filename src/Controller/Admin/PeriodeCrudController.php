<?php

namespace App\Controller\Admin;

use App\Entity\Periode;
use Doctrine\DBAL\Types\DateIntervalType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use Symfony\Component\DomCrawler\Field\FormField;

class PeriodeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Periode::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            IntegerField::new('numPeriode'),
            DateField::new('dateDebut'),
            DateField::new('dateFin')
        ];
    }

}
