<?php

namespace App\Controller\Admin;
use App\Entity\AnneeScolaire;
use App\Entity\HygieneOfficer;
use App\Entity\Periode;
use App\Entity\Professor;
use App\Entity\Tutor;
use App\Entity\User;
use App\Entity\Student;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="adminDashboard")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(): Response
    {
        return $this->render('bundles/EasyAdminBundle/welcome.html.twig',[
            'user' =>[]
        ]);

    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
        -> setTitle('PascalSchool')

            ;

    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('utilisateurs', 'fa fa-users', User::class);
        yield MenuItem::linkToCrud('élève', 'fa fa-child', Student::class);
        yield MenuItem::linkToCrud('Enseignant', 'fa fa-user-circle-o', Professor::class);
        yield MenuItem::linkToCrud('Tuteur', 'fa fa-user-o', Tutor::class);
        yield MenuItem::linkToCrud('Agent hygiéne', 'fa fa-heartbeat', HygieneOfficer::class);

        yield MenuItem::linkToCrud('Année Scolaire', 'fa fa-level-up', AnneeScolaire::class);
        yield MenuItem::linkToCrud('Périodes', 'fa fa-level-up', Periode::class);
        yield MenuItem::linkToCrud('Niveaux', 'fa fa-level-up', HygieneOfficer::class);
        yield MenuItem::linkToCrud('Matiéres', 'fa fa-file-text', HygieneOfficer::class);
        yield MenuItem::linkToCrud('Emploi du temps', 'fa fa-list-alt', HygieneOfficer::class);
        yield MenuItem::linkToCrud('Examens', 'fa fa-newspaper-o', HygieneOfficer::class);
        yield MenuItem::linkToCrud('Absences', 'fa fa-save', HygieneOfficer::class);
        yield MenuItem::linkToCrud('salles', 'fa fa-university', HygieneOfficer::class);
        yield MenuItem::linkToCrud('Message', 'fa fa-envelope-open-o', HygieneOfficer::class);
        yield MenuItem::linkToCrud('Actualités', 'fa fa-bullhorn', HygieneOfficer::class);
        yield MenuItem::linkToCrud('Paiements', 'fa fa-dollar', HygieneOfficer::class);

    }

}