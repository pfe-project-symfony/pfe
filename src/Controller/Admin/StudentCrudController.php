<?php

namespace App\Controller\Admin;


use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use App\Field\VichImageField;
use App\Entity\Student;
use Vich\UploaderBundle\Form\Type\VichImageType;

class StudentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Student::class;
    }
    public function configureActions(Actions $actions): Actions
    {
        $detailUser=Action::new('detailUser','Detail','fa fa-eye')
            ->linkToCrudAction(Crud::PAGE_DETAIL)
            ->addCssClass('btn btn-outline-primary');




        return $actions
            ->setPermission(Action::DELETE,'ROLE_ADMIN')
            # ->disable(Action::EDIT)
            ->add(Crud::PAGE_INDEX,$detailUser)
            ->update(crud::PAGE_INDEX,Action::NEW,function(Action $action){
                return $action->setIcon('fa fa-user')->addCssClass('btn btn-warning');
            })
            ->update(crud::PAGE_INDEX,Action::EDIT,function(Action $action){
                return $action->setIcon('fa fa-edit')->addCssClass('btn btn-outline-success');
            })
            ->update(crud::PAGE_INDEX,Action::DELETE,function(Action $action) {
                return $action->setIcon('fa fa-trash')->addCssClass('btn btn-outline-danger');
            });

    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('userName');
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')
                ->onlyOnIndex(),
            TextField::new('userName','login')->hideOnIndex(),
            TextField::new('password','Mot de passe')
                ->hideOnIndex()
                ->onlyWhenUpdating(),
            ChoiceField::new('roles', 'Role')->hideOnIndex()->hideOnDetail()
                ->allowMultipleChoices()
                ->autocomplete()
                ->setChoices(['Student' => 'ROLE_STUDENT']),
            TextField::new('lastName','Nom')
                ->hideOnIndex(),
            TextField::new('firstName','Prénom'),
            DateField::new('birthday','Date de naissance'),
            ChoiceField::new('gender', 'sexe')
                ->allowMultipleChoices(false)
                ->autocomplete()
                ->setChoices(['Homme' =>'Homme',
                              'Femme' => 'Femme'
                              ])->hideOnIndex(),


            TextField::new('address','Adresse')->hideOnIndex(),
            TextField::new('InformationsMedicales')->hideOnIndex(),
//            ChoiceField::new('Niveau')
//                ->allowMultipleChoices(false)
//                ->autocomplete()
//                ->setChoices([  '1ére année'=>'1',
//                   '2éme année'=> '2',
//                    '3éme année'=>'3',
//                    '4éme année'=>'4',
//                    '5éme année'=>'5',
//                    '6éme année'=>'6']),
//            ChoiceField::new('Classe')
//                ->allowMultipleChoices(false)
//                ->autocomplete()
//                ->setChoices([  '1ére année A'=>'1a',
//                    '1ére année B'=>'1b',
//                    '1ére année C'=>'1c',
//                    '2éme année A'=> '2a',
//                    '2éme année B'=> '2b',
//                    '2éme année C'=> '2c',
//                    '3éme année A'=>'3a',
//                    '3éme année B'=>'3b',
//                    '3éme année C'=>'3c',
//                    '3éme année D'=>'3d',
//                    '4éme année A'=>'4a',
//                    '4éme année B'=>'4b',
//                    '5éme année A'=>'5a',
//                    '5éme année B'=>'5b',
//                    '6éme année A'=>'6a']),

            EmailField::new('email')->hideOnForm(),
            TelephoneField::new('tel','Téléphone'),
            AssociationField::new('tutor','Tuteur'),
            TextareaField::new('photoFile')
                ->setFormType(VichImageType::class)->onlyOnForms(),
            ImageField::new('photo')
                ->setBasePath('uploads/files') ->setUploadDir('public/uploads/files')
                ->setLabel('Image')->onlyOnDetail()
        ];
    }

}
