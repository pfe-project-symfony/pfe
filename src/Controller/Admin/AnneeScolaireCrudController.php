<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;

use App\Entity\AnneeScolaire;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AnneeScolaireCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return AnneeScolaire::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id','ID')->hideOnForm(),
            IntegerField::new('debutAnnee',"Debut d'année")->onlyOnForms(),
            IntegerField::new('finAnnee',"Fin d'année")->onlyOnForms(),
            TextField::new('libelle','Libellé')->hideOnForm(),
        ];
    }

}
