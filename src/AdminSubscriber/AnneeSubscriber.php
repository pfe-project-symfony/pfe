<?php


namespace App\AdminSubscriber;


use App\Entity\AnneeScolaire;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AnneeSubscriber implements EventSubscriberInterface
{

    /**
     * AnneeSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['AjouterAnnee']
        ];
    }
        /**
         * @param BeforeEntityPersistedEvent $event
         */
        public function AjouterAnnee(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof AnneeScolaire){
            $debutAnnee = $entity->getDebutAnnee();
            $finAnnee = $entity->getFinAnnee();
            $lib = $debutAnnee.'-'.$finAnnee;
            $entity->setLibelle($lib);
        }
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

}