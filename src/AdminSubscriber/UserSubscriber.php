<?php


namespace App\AdminSubscriber;

use App\Entity\Student;
use App\Services\PasswordGenerator;
use App\Services\SwiftMailerService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use App\Entity\Contact;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class UserSubscriber implements EventSubscriberInterface
{
    const TEMPLATE_CONTACT = "email/contact.html.twig";

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var SwiftMailerService
     */
    private $mailerService;


    public function __construct(EntityManagerInterface $entityManager, PasswordGenerator $passwordGenerator, UserPasswordEncoderInterface $passwordEncoder, SwiftMailerService $mailerService)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->mailerService = $mailerService;
        $this->passwordGenerator = $passwordGenerator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => ['addUser'],
//            BeforeEntityUpdatedEvent::class => ['updateUser'] //surtout utile lors d'un reset de mot passe plutôt qu'un réel update, car l'update va de nouveau encrypter le mot de passe DEJA encrypté ...
        ];
    }

//    public function updateUser(BeforeEntityUpdatedEvent $event)
//    {
//        $entity = $event->getEntityInstance();
//
//        if ($entity instanceof User) {
//
//            //  $this->cryptPassword($entity);
//
//
//            // set new password with encoder interface
//            if (method_exists($entity, 'setPassword')) {
//
//                $plainPwd = $this->passwordGenerator->generateRandomStrongPassword(8);
//                //$clearPassword = trim($this->get('request_stack')->getCurrentRequest()->request->all()['User']['password']);
//                // save password only if is set a new clearpass
//                if (!empty($plainPwd)) {
//                    // $encodedPassword = $this->passwordEncoder->encodePassword($this->getUser(), $plainPwd);
//                    //$entity->setPassword($encodedPassword);
//                    //******** send mail to the user containing the plain password  ***********
//                    $contact = $this->createContact($entity, $plainPwd);
//                    try {
//                        $this->sendContact($contact);
//                    } catch (LoaderError | RuntimeError | SyntaxError $e) {
//                    }
//
//                    //******** encrypt the generated password ($plainPwd) and persist the User entity in DB *****
//                    $this->cryptPassword($entity, $plainPwd);
//                }
//            }
//        }
//    }
    /**
     * @param BeforeEntityPersistedEvent $event
     */
    public function addUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof User) {
            $firstName = $entity->getFirstName();
            $random = $this->randomNumbers();
            $username = $firstName.$random;
            $entity->setUserName($username);
            //generate random password
            $plainPwd = $this->passwordGenerator->generateRandomStrongPassword(8);

            //******* Student mail *********
            if($entity instanceof Student){
                //set student email ( the tutor's one)
                $tutorMail = $entity->getTutorEmail();
                $entity->setEmail($tutorMail);
            }
            //*******/. Student mail

            //send mail to the user containing the plain password
            $contact = $this->createContact($entity, $plainPwd);
            try {
                $this->sendContact($contact);
            } catch (LoaderError | RuntimeError | SyntaxError $e) {
            }

            //encrypt the generated password ($plainPwd)
            $this->cryptPassword($entity, $plainPwd);
            //persisting in DB
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }
    }

    /**
     * @param User $entity
     * @param string $plainPwd
     * @return Contact
     */
    public function createContact(User $entity,string $plainPwd): Contact {

        //get data
        $firstName=$entity->getFirstName();
        $lastName=$entity->getLastName();
        $userName = $entity->getUserName();

        $name = $firstName.' '.$lastName ;
        $email=$entity->getEmail();
        $message= sprintf("le login du compte:%s \n le mot de passe du compte:%s",$userName,$plainPwd) ;

        //create Contact entity
        $contact= new Contact;
        $contact->setName($name);
        $contact->setEmail($email);
        $contact->setDescription($message);
        return $contact;

    }
    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendContact(Contact $contact)
    {
        $TEMPLATE_CONTACT = "email/contact.html.twig";
        $parameters = [
            "email" => $contact->getEmail(),
            "name" => $contact->getName(),
            "description" => $contact->getDescription()
        ];

        $this->mailerService->send(
            "Pascal school, mot de passe de votre compte",
            ['pascal.Primary.School@gmail.com'],
            [$contact->getEmail()],
            $TEMPLATE_CONTACT,
            $parameters
        );
    }


    /**
     * @param User $entity
     * @param string $plainPwd
     */
    public function cryptPassword(User $entity,string $plainPwd): void
    {
        $entity->setPassword(
            $this->passwordEncoder->encodePassword(
                $entity,
                $plainPwd
            )
        );

    }

    public function randomNumbers(): string
    {
        //returns a string of 4 random numbers :
        //It generates a random 4 character string consisting of, by default, only 0-9,
        // but you can change the value of $a for other characters. The random string will be in variable $s .

        for ($s = '', $i = 0, $z = strlen($a = '0123456789')-1; $i != 4; $x = rand(0,$z), $s .= $a{$x}, $i++);
        return $s;
    }


}