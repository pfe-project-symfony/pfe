<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* tutor.html.twig */
class __TwigTemplate_d570bfb0785388408ae997ccf4f3ad679c14536a5441dcc96c9ae79d68ec158f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'main_sidebar_container' => [$this, 'block_main_sidebar_container'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "tutor.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "tutor.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "tutor.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " Tutor | Dashboard";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_main_sidebar_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_sidebar_container"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_sidebar_container"));

        // line 5
        echo "        <div class=\"login-header \">
            <a  class=\"logo\">
                <img src=\"https://myeducpro.com/assets/login_page/img/logo.png\" height=\"40\" alt=\"School logo\">
            </a>

            <h3 class=\"title\" text-align=\" center\">Pascal School</h3>

        </div>
        <!-- Sidebar -->
        <div class=\"sidebar\">
            <!-- Sidebar user panel (optional) -->


            <!-- Sidebar Menu -->
            <nav class=\"mt-2\">
                <ul class=\"nav nav-pills nav-sidebar flex-column\" data-widget=\"treeview\" role=\"menu\" data-accordion=\"false\">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class=\"nav-item has-treeview menu-open\">
                        <a href=\"#\" class=\"nav-link active\">
                            <i class=\"nav-icon fas fa-tachometer-alt\"></i>
                            <p>
                                Accueil
                                <!-- <i class=\"right fas fa-angle-left\"></i> -->
                            </p>
                        </a>

                    </li>

                    <li class=\"nav-item \">
                        <a href=\"#\" class=\"nav-link\">
                            <p>
                                <i class=\"nav-icon fa fa-users\"></i>
                                Enseignants
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item \">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fa fa-clipboard\"></i>
                            <p>
                                Matières
                            </p>
                        </a>
                    </li>
                    <li class=\"nav-item \">
                        <a href=\"#\" class=\"nav-link\">
                            <p>
                                <i class=\"nav-icon far fa-calendar-alt\"></i>
                                Emploi du temps
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-edit\"></i>
                            <p>
                                Devoirs de maison
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-chart-bar\"></i>
                            <p>
                                Absences
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-chart-bar\"></i>
                            <p>
                                Punitions
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-chart-line\"></i>
                            <p>
                                Appréciations
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-file-alt\"></i>
                            <p>
                                Bulletins de notes
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-book-reader\"></i>
                            <p>
                                Bibliothèque
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-baseball-ball\"></i>
                            <p>
                                Actualités et évènements
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-donate\"></i>
                            <p>
                                Paiement
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item \">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon far fa-envelope\"></i>
                            <p>
                                Messages
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                        <ul class=\"nav nav-treeview\">
                            <li class=\"nav-item\">
                                <a href=\"pages/mailbox/mailbox.html\" class=\"nav-link\">
                                    <i class=\"far fa-circle nav-icon\"></i>
                                    <p>Inbox</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-lock\"></i>
                            <p>
                                Compte
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>

                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "tutor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 5,  78 => 4,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block title %} Tutor | Dashboard{% endblock %}

    {% block main_sidebar_container %}
        <div class=\"login-header \">
            <a  class=\"logo\">
                <img src=\"https://myeducpro.com/assets/login_page/img/logo.png\" height=\"40\" alt=\"School logo\">
            </a>

            <h3 class=\"title\" text-align=\" center\">Pascal School</h3>

        </div>
        <!-- Sidebar -->
        <div class=\"sidebar\">
            <!-- Sidebar user panel (optional) -->


            <!-- Sidebar Menu -->
            <nav class=\"mt-2\">
                <ul class=\"nav nav-pills nav-sidebar flex-column\" data-widget=\"treeview\" role=\"menu\" data-accordion=\"false\">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class=\"nav-item has-treeview menu-open\">
                        <a href=\"#\" class=\"nav-link active\">
                            <i class=\"nav-icon fas fa-tachometer-alt\"></i>
                            <p>
                                Accueil
                                <!-- <i class=\"right fas fa-angle-left\"></i> -->
                            </p>
                        </a>

                    </li>

                    <li class=\"nav-item \">
                        <a href=\"#\" class=\"nav-link\">
                            <p>
                                <i class=\"nav-icon fa fa-users\"></i>
                                Enseignants
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item \">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fa fa-clipboard\"></i>
                            <p>
                                Matières
                            </p>
                        </a>
                    </li>
                    <li class=\"nav-item \">
                        <a href=\"#\" class=\"nav-link\">
                            <p>
                                <i class=\"nav-icon far fa-calendar-alt\"></i>
                                Emploi du temps
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-edit\"></i>
                            <p>
                                Devoirs de maison
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-chart-bar\"></i>
                            <p>
                                Absences
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-chart-bar\"></i>
                            <p>
                                Punitions
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-chart-line\"></i>
                            <p>
                                Appréciations
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-file-alt\"></i>
                            <p>
                                Bulletins de notes
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-book-reader\"></i>
                            <p>
                                Bibliothèque
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-baseball-ball\"></i>
                            <p>
                                Actualités et évènements
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-donate\"></i>
                            <p>
                                Paiement
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                    </li>

                    <li class=\"nav-item \">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon far fa-envelope\"></i>
                            <p>
                                Messages
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>
                        <ul class=\"nav nav-treeview\">
                            <li class=\"nav-item\">
                                <a href=\"pages/mailbox/mailbox.html\" class=\"nav-link\">
                                    <i class=\"far fa-circle nav-icon\"></i>
                                    <p>Inbox</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class=\"nav-item\">
                        <a href=\"#\" class=\"nav-link\">
                            <i class=\"nav-icon fas fa-lock\"></i>
                            <p>
                                Compte
                                <i class=\"fas fa-angle-right  right\"></i>
                            </p>
                        </a>

                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    {% endblock %}", "tutor.html.twig", "D:\\Roua\\phpstormProjects\\PFE_Project\\templates\\tutor.html.twig");
    }
}
