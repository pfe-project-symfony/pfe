<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @EasyAdmin/welcome.html.twig */
class __TwigTemplate_18d00bfecc5f95665f780454dbdaf865146b6a0b1d47ddb568c3b7e8b7f0cdd1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'page_title' => [$this, 'block_page_title'],
            'page_content' => [$this, 'block_page_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "@EasyAdmin/page/content.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@EasyAdmin/welcome.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@EasyAdmin/welcome.html.twig"));

        $this->parent = $this->loadTemplate("@EasyAdmin/page/content.html.twig", "@EasyAdmin/welcome.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_page_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_title"));

        echo "ADMIN";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_page_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "page_content"));

        // line 7
        echo "

<!-- Main content -->
<!--<section class=\"content\">-->
    <div class=\"container-fluid\">
        <!-- Small boxes (Stat box) -->
        <div class=\"row\">
            <div class=\"col-lg-3 col-6\">
                <!-- small box -->
                <div class=\"small-box bg-info\">
                    <div class=\"inner\">
                        <h3>150</h3>

                        <p>Utilisateurs</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-bag\"></i>
                    </div>

                </div>
            </div>
            <!-- ./col -->
            <div class=\"col-lg-3 col-6\">
                <!-- small box -->
                <div class=\"small-box bg-success\">
                    <div class=\"inner\">
                        <h3>53<sup style=\"font-size: 20px\"></sup></h3>

                        <p>élève</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-stats-bars\"></i>
                    </div>

                </div>
            </div>
            <!-- ./col -->
            <div class=\"col-lg-3 col-6\">
                <!-- small box -->
                <div class=\"small-box bg-warning\">
                    <div class=\"inner\">
                        <h3>44</h3>

                        <p>Enseignant</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-person-add\"></i>
                    </div>

                </div>
            </div>
            <!-- ./col -->
            <div class=\"col-lg-3 col-6\">
                <!-- small box -->
                <div class=\"small-box bg-danger\">
                    <div class=\"inner\">
                        <h3>65</h3>

                        <p>Agent d'hygiéne</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-pie-graph\"></i>
                    </div>

                </div>
            </div>
            <!-- ./col -->
        </div>
    </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/welcome.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 7,  78 => 6,  59 => 4,  36 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{# @var ea \\EasyCorp\\Bundle\\EasyAdminBundle\\Context\\AdminContext #}
{% extends '@EasyAdmin/page/content.html.twig' %}

{% block page_title 'ADMIN' %}

{% block page_content %}


<!-- Main content -->
<!--<section class=\"content\">-->
    <div class=\"container-fluid\">
        <!-- Small boxes (Stat box) -->
        <div class=\"row\">
            <div class=\"col-lg-3 col-6\">
                <!-- small box -->
                <div class=\"small-box bg-info\">
                    <div class=\"inner\">
                        <h3>150</h3>

                        <p>Utilisateurs</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-bag\"></i>
                    </div>

                </div>
            </div>
            <!-- ./col -->
            <div class=\"col-lg-3 col-6\">
                <!-- small box -->
                <div class=\"small-box bg-success\">
                    <div class=\"inner\">
                        <h3>53<sup style=\"font-size: 20px\"></sup></h3>

                        <p>élève</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-stats-bars\"></i>
                    </div>

                </div>
            </div>
            <!-- ./col -->
            <div class=\"col-lg-3 col-6\">
                <!-- small box -->
                <div class=\"small-box bg-warning\">
                    <div class=\"inner\">
                        <h3>44</h3>

                        <p>Enseignant</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-person-add\"></i>
                    </div>

                </div>
            </div>
            <!-- ./col -->
            <div class=\"col-lg-3 col-6\">
                <!-- small box -->
                <div class=\"small-box bg-danger\">
                    <div class=\"inner\">
                        <h3>65</h3>

                        <p>Agent d'hygiéne</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-pie-graph\"></i>
                    </div>

                </div>
            </div>
            <!-- ./col -->
        </div>
    </div>
    </div>
{% endblock %}
", "@EasyAdmin/welcome.html.twig", "D:\\Roua\\phpstormProjects\\PFE_Project\\templates\\bundles\\EasyAdminBundle\\welcome.html.twig");
    }
}
