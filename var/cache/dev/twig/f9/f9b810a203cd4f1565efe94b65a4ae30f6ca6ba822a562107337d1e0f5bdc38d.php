<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* hygiene_officer.html.twig */
class __TwigTemplate_922a433bcddb5e4ffbbf0cb42bdd8970aed1587ddc0d021a3cf69493c0a4d635 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'main_sidebar_container' => [$this, 'block_main_sidebar_container'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hygiene_officer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hygiene_officer.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "hygiene_officer.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "HygieneOfficer| Dashboard";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_main_sidebar_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_sidebar_container"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "main_sidebar_container"));

        // line 4
        echo "    <div class=\"login-header \">
        <a  class=\"logo\">
            <img src=\"https://myeducpro.com/assets/login_page/img/logo.png\" height=\"40\" alt=\"School logo\">
        </a>

        <h3 class=\"title\" text-align=\"left\">Pascal School</h3>

    </div>
    <!-- Sidebar -->
    <div class=\"sidebar\">
        <!-- Sidebar user panel (optional) -->


        <!-- Sidebar Menu -->
        <nav class=\"mt-2\">
            <ul class=\"nav nav-pills nav-sidebar flex-column\" data-widget=\"treeview\" role=\"menu\" data-accordion=\"false\">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class=\"nav-item has-treeview menu-open\">
                    <a href=\"#\" class=\"nav-link active\">
                        <i class=\"nav-icon fas fa-tachometer-alt\"></i>
                        <p>
                            Accueil
                            <!-- <i class=\"right fas fa-angle-left\"></i> -->
                        </p>
                    </a>

                </li>

                <li class=\"nav-item\">
                    <a href=\"#\" class=\"nav-link\">
                        <i class=\"nav-icon fas fa-book-reader\"></i>
                        <p>
                            Bibliothèque
                            <i class=\"fas fa-angle-right  right\"></i>
                        </p>
                    </a>

                </li>
                <li class=\"nav-item\">
                    <a href=\"#\" class=\"nav-link\">
                        <i class=\"nav-icon fas fa-plus-square\"></i>
                        <p>
                            Actualités et évènements
                            <i class=\"fas fa-angle-right  right\"></i>
                        </p>
                    </a>

                </li>
                <li class=\"nav-item \">
                    <a href=\"#\" class=\"nav-link\">
                        <i class=\"nav-icon far fa-envelope\"></i>
                        <p>
                            Messages
                            <i class=\"fas fa-angle-right  right\"></i>
                        </p>
                    </a>
                    <ul class=\"nav nav-treeview\">
                        <li class=\"nav-item\">
                            <a href=\"pages/mailbox/mailbox.html\" class=\"nav-link\">
                                <i class=\"far fa-circle nav-icon\"></i>
                                <p>Inbox</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class=\"nav-item\">
                    <a href=\"#\" class=\"nav-link\">
                        <i class=\"nav-icon fas fa-lock\"></i>
                        <p>
                            Compte
                            <i class=\"fas fa-angle-right  right\"></i>
                        </p>
                    </a>

                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "hygiene_officer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 4,  78 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block title %}HygieneOfficer| Dashboard{% endblock %}
{% block main_sidebar_container %}
    <div class=\"login-header \">
        <a  class=\"logo\">
            <img src=\"https://myeducpro.com/assets/login_page/img/logo.png\" height=\"40\" alt=\"School logo\">
        </a>

        <h3 class=\"title\" text-align=\"left\">Pascal School</h3>

    </div>
    <!-- Sidebar -->
    <div class=\"sidebar\">
        <!-- Sidebar user panel (optional) -->


        <!-- Sidebar Menu -->
        <nav class=\"mt-2\">
            <ul class=\"nav nav-pills nav-sidebar flex-column\" data-widget=\"treeview\" role=\"menu\" data-accordion=\"false\">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class=\"nav-item has-treeview menu-open\">
                    <a href=\"#\" class=\"nav-link active\">
                        <i class=\"nav-icon fas fa-tachometer-alt\"></i>
                        <p>
                            Accueil
                            <!-- <i class=\"right fas fa-angle-left\"></i> -->
                        </p>
                    </a>

                </li>

                <li class=\"nav-item\">
                    <a href=\"#\" class=\"nav-link\">
                        <i class=\"nav-icon fas fa-book-reader\"></i>
                        <p>
                            Bibliothèque
                            <i class=\"fas fa-angle-right  right\"></i>
                        </p>
                    </a>

                </li>
                <li class=\"nav-item\">
                    <a href=\"#\" class=\"nav-link\">
                        <i class=\"nav-icon fas fa-plus-square\"></i>
                        <p>
                            Actualités et évènements
                            <i class=\"fas fa-angle-right  right\"></i>
                        </p>
                    </a>

                </li>
                <li class=\"nav-item \">
                    <a href=\"#\" class=\"nav-link\">
                        <i class=\"nav-icon far fa-envelope\"></i>
                        <p>
                            Messages
                            <i class=\"fas fa-angle-right  right\"></i>
                        </p>
                    </a>
                    <ul class=\"nav nav-treeview\">
                        <li class=\"nav-item\">
                            <a href=\"pages/mailbox/mailbox.html\" class=\"nav-link\">
                                <i class=\"far fa-circle nav-icon\"></i>
                                <p>Inbox</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class=\"nav-item\">
                    <a href=\"#\" class=\"nav-link\">
                        <i class=\"nav-icon fas fa-lock\"></i>
                        <p>
                            Compte
                            <i class=\"fas fa-angle-right  right\"></i>
                        </p>
                    </a>

                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
{% endblock %}
", "hygiene_officer.html.twig", "D:\\Chadha\\Applications\\PhpstormProjects\\PFE_Project\\templates\\hygiene_officer.html.twig");
    }
}
