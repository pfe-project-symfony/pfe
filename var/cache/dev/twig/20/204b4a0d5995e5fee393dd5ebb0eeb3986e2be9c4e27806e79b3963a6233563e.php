<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_de487e8f37a5c21353b78ed923d0c7c562dc293ea5700c13df3964be071e9570 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <!--========== BOX ICONS ==========-->
    <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css\">

    <!--========== CSS ==========-->
    <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/styles.css"), "html", null, true);
        echo "\">

    <title>Responsive sidebar submenus</title>
</head>
<body>
<!--========== HEADER ==========-->
<header class=\"header\">
    <div class=\"header__container\">
        <img src=\"../assets/img/profil.jpg\" alt=\"\" class=\"header__img\">

        <a href=\"#\" class=\"header__logo\">Bedimcode</a>

        <div class=\"header__search\">
            <input type=\"search\" placeholder=\"Search\" class=\"header__input\">
            <i class='bx bx-search header__icon'></i>
        </div>

        <div class=\"header__toggle\">
            <i class='bx bx-menu' id=\"header-toggle\"></i>
        </div>
    </div>
</header>

<!--========== NAV ==========-->
<div class=\"nav\" id=\"navbar\">
    <nav class=\"nav__container\">
        <div>
            <a href=\"#\" class=\"nav__link nav__logo\">
                <i class='bx bxs-disc nav__icon' ></i>
                <span class=\"nav__logo-name\">Bedimcode</span>
            </a>

            <div class=\"nav__list\">
                <div class=\"nav__items\">
                    <h3 class=\"nav__subtitle\">Profile</h3>

                    <a href=\"#\" class=\"nav__link active\">
                        <i class='bx bx-home nav__icon' ></i>
                        <span class=\"nav__name\">Home</span>
                    </a>

                    <div class=\"nav__dropdown\">
                        <a href=\"#\" class=\"nav__link\">
                            <i class='bx bx-user nav__icon' ></i>
                            <span class=\"nav__name\">Profile</span>
                            <i class='bx bx-chevron-down nav__icon nav__dropdown-icon'></i>
                        </a>

                        <div class=\"nav__dropdown-collapse\">
                            <div class=\"nav__dropdown-content\">
                                <a href=\"#\" class=\"nav__dropdown-item\">Passwords</a>
                                <a href=\"#\" class=\"nav__dropdown-item\">Mail</a>
                                <a href=\"#\" class=\"nav__dropdown-item\">Accounts</a>
                            </div>
                        </div>
                    </div>

                    <a href=\"#\" class=\"nav__link\">
                        <i class='bx bx-message-rounded nav__icon' ></i>
                        <span class=\"nav__name\">Messages</span>
                    </a>
                </div>

                <div class=\"nav__items\">
                    <h3 class=\"nav__subtitle\">Menu</h3>

                    <div class=\"nav__dropdown\">
                        <a href=\"#\" class=\"nav__link\">
                            <i class='bx bx-bell nav__icon' ></i>
                            <span class=\"nav__name\">Notifications</span>
                            <i class='bx bx-chevron-down nav__icon nav__dropdown-icon'></i>
                        </a>

                        <div class=\"nav__dropdown-collapse\">
                            <div class=\"nav__dropdown-content\">
                                <a href=\"#\" class=\"nav__dropdown-item\">Blocked</a>
                                <a href=\"#\" class=\"nav__dropdown-item\">Silenced</a>
                                <a href=\"#\" class=\"nav__dropdown-item\">Publish</a>
                                <a href=\"#\" class=\"nav__dropdown-item\">Program</a>
                            </div>
                        </div>

                    </div>

                    <a href=\"#\" class=\"nav__link\">
                        <i class='bx bx-compass nav__icon' ></i>
                        <span class=\"nav__name\">Explore</span>
                    </a>
                    <a href=\"#\" class=\"nav__link\">
                        <i class='bx bx-bookmark nav__icon' ></i>
                        <span class=\"nav__name\">Saved</span>
                    </a>
                </div>
            </div>
        </div>

        <a href=\"#\" class=\"nav__link nav__logout\">
            <i class='bx bx-log-out nav__icon' ></i>
            <span class=\"nav__name\">Log Out</span>
        </a>
    </nav>
</div>

<!--========== CONTENTS ==========-->
<main>
    <section>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt vel illum fuga unde cum, voluptates magni molestias eveniet culpa autem ut, totam veniam, suscipit tempore ullam pariatur est at asperiores?</p>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt vel illum fuga unde cum, voluptates magni molestias eveniet culpa autem ut, totam veniam, suscipit tempore ullam pariatur est at asperiores?</p>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt vel illum fuga unde cum, voluptates magni molestias eveniet culpa autem ut, totam veniam, suscipit tempore ullam pariatur est at asperiores?</p>
    </section>
</main>

<!--========== MAIN JS ==========-->
<script src=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/js/main.js"), "html", null, true);
        echo "\"></script>
</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 124,  55 => 11,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <!--========== BOX ICONS ==========-->
    <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css\">

    <!--========== CSS ==========-->
    <link rel=\"stylesheet\" href=\"{{ asset('build/css/styles.css') }}\">

    <title>Responsive sidebar submenus</title>
</head>
<body>
<!--========== HEADER ==========-->
<header class=\"header\">
    <div class=\"header__container\">
        <img src=\"../assets/img/profil.jpg\" alt=\"\" class=\"header__img\">

        <a href=\"#\" class=\"header__logo\">Bedimcode</a>

        <div class=\"header__search\">
            <input type=\"search\" placeholder=\"Search\" class=\"header__input\">
            <i class='bx bx-search header__icon'></i>
        </div>

        <div class=\"header__toggle\">
            <i class='bx bx-menu' id=\"header-toggle\"></i>
        </div>
    </div>
</header>

<!--========== NAV ==========-->
<div class=\"nav\" id=\"navbar\">
    <nav class=\"nav__container\">
        <div>
            <a href=\"#\" class=\"nav__link nav__logo\">
                <i class='bx bxs-disc nav__icon' ></i>
                <span class=\"nav__logo-name\">Bedimcode</span>
            </a>

            <div class=\"nav__list\">
                <div class=\"nav__items\">
                    <h3 class=\"nav__subtitle\">Profile</h3>

                    <a href=\"#\" class=\"nav__link active\">
                        <i class='bx bx-home nav__icon' ></i>
                        <span class=\"nav__name\">Home</span>
                    </a>

                    <div class=\"nav__dropdown\">
                        <a href=\"#\" class=\"nav__link\">
                            <i class='bx bx-user nav__icon' ></i>
                            <span class=\"nav__name\">Profile</span>
                            <i class='bx bx-chevron-down nav__icon nav__dropdown-icon'></i>
                        </a>

                        <div class=\"nav__dropdown-collapse\">
                            <div class=\"nav__dropdown-content\">
                                <a href=\"#\" class=\"nav__dropdown-item\">Passwords</a>
                                <a href=\"#\" class=\"nav__dropdown-item\">Mail</a>
                                <a href=\"#\" class=\"nav__dropdown-item\">Accounts</a>
                            </div>
                        </div>
                    </div>

                    <a href=\"#\" class=\"nav__link\">
                        <i class='bx bx-message-rounded nav__icon' ></i>
                        <span class=\"nav__name\">Messages</span>
                    </a>
                </div>

                <div class=\"nav__items\">
                    <h3 class=\"nav__subtitle\">Menu</h3>

                    <div class=\"nav__dropdown\">
                        <a href=\"#\" class=\"nav__link\">
                            <i class='bx bx-bell nav__icon' ></i>
                            <span class=\"nav__name\">Notifications</span>
                            <i class='bx bx-chevron-down nav__icon nav__dropdown-icon'></i>
                        </a>

                        <div class=\"nav__dropdown-collapse\">
                            <div class=\"nav__dropdown-content\">
                                <a href=\"#\" class=\"nav__dropdown-item\">Blocked</a>
                                <a href=\"#\" class=\"nav__dropdown-item\">Silenced</a>
                                <a href=\"#\" class=\"nav__dropdown-item\">Publish</a>
                                <a href=\"#\" class=\"nav__dropdown-item\">Program</a>
                            </div>
                        </div>

                    </div>

                    <a href=\"#\" class=\"nav__link\">
                        <i class='bx bx-compass nav__icon' ></i>
                        <span class=\"nav__name\">Explore</span>
                    </a>
                    <a href=\"#\" class=\"nav__link\">
                        <i class='bx bx-bookmark nav__icon' ></i>
                        <span class=\"nav__name\">Saved</span>
                    </a>
                </div>
            </div>
        </div>

        <a href=\"#\" class=\"nav__link nav__logout\">
            <i class='bx bx-log-out nav__icon' ></i>
            <span class=\"nav__name\">Log Out</span>
        </a>
    </nav>
</div>

<!--========== CONTENTS ==========-->
<main>
    <section>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt vel illum fuga unde cum, voluptates magni molestias eveniet culpa autem ut, totam veniam, suscipit tempore ullam pariatur est at asperiores?</p>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt vel illum fuga unde cum, voluptates magni molestias eveniet culpa autem ut, totam veniam, suscipit tempore ullam pariatur est at asperiores?</p>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt vel illum fuga unde cum, voluptates magni molestias eveniet culpa autem ut, totam veniam, suscipit tempore ullam pariatur est at asperiores?</p>
    </section>
</main>

<!--========== MAIN JS ==========-->
<script src=\"{{ asset('build/js/main.js') }}\"></script>
</body>
</html>", "index.html.twig", "D:\\Chadha\\Applications\\PhpstormProjects\\PFE_Project\\templates\\index.html.twig");
    }
}
