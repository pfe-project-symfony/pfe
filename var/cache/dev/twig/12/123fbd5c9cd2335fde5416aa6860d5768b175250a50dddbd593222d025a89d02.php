<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/login.html.twig */
class __TwigTemplate_dd2bb76fee9db1da2cfca32a933f48cc8172d7c0e3eb2001f1f4d2ca0e2126e1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        // line 1
        echo "<!doctype html>

<html class=\"no-js\" lang=\"\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
    <title>
        Login
    </title>
    <meta name=\"description\" content=\"\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link rel=\"shortcut icon\" href=\"https://myeducpro.com/assets/login_page/img/favicon.png\">
    <link rel=\"stylesheet\" href=\"https://myeducpro.com/assets/css/bootstrap.css\">
    <link rel=\"stylesheet\" href=\"https://myeducpro.com/assets/login_page/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"https://myeducpro.com/assets/login_page/css/normalize.css\">
    <link rel=\"stylesheet\" href=\"https://myeducpro.com/assets/login_page/css/main.css\">
    <link rel=\"stylesheet\" href=\"https://myeducpro.com/assets/login_page/css/style.css\">
    <script src=\"https://myeducpro.com/assets/login_page/js/vendor/modernizr-2.8.3.min.js\"></script>
    <link href=\"https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700\" rel=\"stylesheet\">

</head>
<body>
<div class=\"main-content-wrapper\">
    <div class=\"login-area\">
        <div class=\"login-header\">
            <a  class=\"logo\">
                <img src=\"https://myeducpro.com/assets/login_page/img/logo.png\" height=\"60\" alt=\"\">
            </a>
            <h2 class=\"title\">PascalSchool</h2>
            <!--   <h2 class=\"title\">EducPro Solution</h2> -->
        </div>
        <div class=\"login-content\">
            <form method=\"post\" role=\"form\" id=\"form_login\"
                  action=\"";
        // line 35
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("security_login");
        echo "\">
                <div class=\"form-group\">
                    <input type=\"text\" class=\"input-field\" name=\"_username\" placeholder=\"Username...\"
                           required autocomplete=\"off\">
                </div>
                <div class=\"form-group\">
                    <input type=\"password\" class=\"input-field\" name=\"_password\"  placeholder=\"Password...\"
                           required>
                </div>
                <button type=\"submit\" class=\"btn btn-primary\">Connexion<i class=\"fa fa-lock\"></i></button>
                ";
        // line 45
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 45, $this->source); })())) {
            // line 46
            echo "                    <span class=\"error\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 46, $this->source); })()), "messageKey", [], "any", false, false, false, 46), "html", null, true);
            echo "</span>
                ";
        }
        // line 48
        echo "
            </form>

            <div class=\"login-bottom-links\">
                <a href=\"https://myeducpro.com/index.php?login/forgot_password\" class=\"link\">
                    Mot De Pass Oublié ?
                </a>
            </div>

        </div>
    </div>
    <div class=\"image-area\"></div>
</div>
<style>
    .error {color: #fc2626
    }
</style>

<script src=\"https://myeducpro.com/assets/login_page/js/vendor/jquery-1.12.0.min.js\"></script>
<script src=\"https://myeducpro.com/assets/js/bootstrap-notify.js\"></script>



</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 48,  94 => 46,  92 => 45,  79 => 35,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!doctype html>

<html class=\"no-js\" lang=\"\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
    <title>
        Login
    </title>
    <meta name=\"description\" content=\"\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link rel=\"shortcut icon\" href=\"https://myeducpro.com/assets/login_page/img/favicon.png\">
    <link rel=\"stylesheet\" href=\"https://myeducpro.com/assets/css/bootstrap.css\">
    <link rel=\"stylesheet\" href=\"https://myeducpro.com/assets/login_page/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"https://myeducpro.com/assets/login_page/css/normalize.css\">
    <link rel=\"stylesheet\" href=\"https://myeducpro.com/assets/login_page/css/main.css\">
    <link rel=\"stylesheet\" href=\"https://myeducpro.com/assets/login_page/css/style.css\">
    <script src=\"https://myeducpro.com/assets/login_page/js/vendor/modernizr-2.8.3.min.js\"></script>
    <link href=\"https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700\" rel=\"stylesheet\">

</head>
<body>
<div class=\"main-content-wrapper\">
    <div class=\"login-area\">
        <div class=\"login-header\">
            <a  class=\"logo\">
                <img src=\"https://myeducpro.com/assets/login_page/img/logo.png\" height=\"60\" alt=\"\">
            </a>
            <h2 class=\"title\">PascalSchool</h2>
            <!--   <h2 class=\"title\">EducPro Solution</h2> -->
        </div>
        <div class=\"login-content\">
            <form method=\"post\" role=\"form\" id=\"form_login\"
                  action=\"{{ path('security_login') }}\">
                <div class=\"form-group\">
                    <input type=\"text\" class=\"input-field\" name=\"_username\" placeholder=\"Username...\"
                           required autocomplete=\"off\">
                </div>
                <div class=\"form-group\">
                    <input type=\"password\" class=\"input-field\" name=\"_password\"  placeholder=\"Password...\"
                           required>
                </div>
                <button type=\"submit\" class=\"btn btn-primary\">Connexion<i class=\"fa fa-lock\"></i></button>
                {% if error %}
                    <span class=\"error\">{{ error.messageKey }}</span>
                {% endif %}

            </form>

            <div class=\"login-bottom-links\">
                <a href=\"https://myeducpro.com/index.php?login/forgot_password\" class=\"link\">
                    Mot De Pass Oublié ?
                </a>
            </div>

        </div>
    </div>
    <div class=\"image-area\"></div>
</div>
<style>
    .error {color: #fc2626
    }
</style>

<script src=\"https://myeducpro.com/assets/login_page/js/vendor/jquery-1.12.0.min.js\"></script>
<script src=\"https://myeducpro.com/assets/js/bootstrap-notify.js\"></script>



</body>
</html>", "security/login.html.twig", "D:\\Roua\\phpstormProjects\\PFE_Project\\templates\\security\\login.html.twig");
    }
}
